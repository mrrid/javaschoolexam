package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
  public class Piramide {
    private static int canBuild(int n) {
        double d = Math.sqrt(1 + 8 * n);
        if (d % 1 == 0) return(int)((-1+d)/2);
        System.out.println("CannotBuildPyramidException");
        return -1;
    }



    private static int[][] buildPyramid(List<Integer> Arr) {
        int counter=0;
        int n = Arr.size();
            n=canBuild(n);
        int a[][]=new int[n][2*n-1];
        for(int i=1;i<=n;i++){
            int k=1;
            for(int j=1;j<=2*n-1;j++){
                if((j>0&&j<=n-i)||(j>=n+i&&j<=2*n-1)){
                    a[i-1][j-1]=0;
                }else{

                    j--;
                    for(int y=n;y<n+i;y++){
                        if(k%2==0){
                            a[i-1][j-1]=0;
                        }else{
                            a[i-1][j]=Arr.get(counter++);
                        }

                        k++;
                        j++;
                        if(j>=2*n-1) break;
                    }
                }
            }
            System.out.printf("\n");
        }
        return a;
    }


}
