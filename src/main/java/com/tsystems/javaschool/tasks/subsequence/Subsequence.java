package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
public class Subsequence {
    public static boolean sub(List<String> arrMin, List<String> arrMax){
        int currentIndex=0,min=arrMin.size(),max=arrMax.size();
        for(int i=0;i<min-1;i++){
            int j= currentIndex;
            while(true){
                if(arrMax.get(j).equals(arrMin.get(i))){
                    currentIndex=++j;
                    break;
                }else if(j>=max-1){
                    return false;
                }else j++;

            }
        }

        return true;
    }
    public static boolean fine(List<String> x, List<String> y){
        if(x.size()< y.size())return sub(x, y);
        else return sub(y, x);
    }

}
