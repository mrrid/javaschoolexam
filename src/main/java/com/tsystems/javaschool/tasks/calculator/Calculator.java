package com.tsystems.javaschool.tasks.calculator;
import java.util.*;
public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
     
      public static final Map<String, Integer> MAIN_MATH_OPERATIONS;
    public static final Set<String> NUMBER;

    static {
        MAIN_MATH_OPERATIONS = new HashMap<String, Integer>();
        MAIN_MATH_OPERATIONS.put("*", 3);
        MAIN_MATH_OPERATIONS.put("/", 3);
        MAIN_MATH_OPERATIONS.put("+", 2);
        MAIN_MATH_OPERATIONS.put("-", 2);
        MAIN_MATH_OPERATIONS.put("(", 1);
        MAIN_MATH_OPERATIONS.put(")", 1);
    }

    static {
        NUMBER = new HashSet<String>();
        NUMBER.add(".");
        NUMBER.add("0");
        NUMBER.add("1");
        NUMBER.add("2");
        NUMBER.add("3");
        NUMBER.add("4");
        NUMBER.add("5");
        NUMBER.add("6");
        NUMBER.add("7");
        NUMBER.add("8");
        NUMBER.add("9");

    }

    public static boolean stackADD(Stack<String> stack, char c) {
        if (stack.isEmpty() || (MAIN_MATH_OPERATIONS.get(stack.peek()) < MAIN_MATH_OPERATIONS.get(Character.toString(c)) || c == '(')) {
            return true;
        } else return false;
    }

    public static Double computation(List<String> Str) {
        Stack<Double> stack = new Stack<>();
        for (int i = 0; i < Str.size(); i++) {
            if (!MAIN_MATH_OPERATIONS.containsKey(Str.get(i))) {
                stack.push(Double.parseDouble(Str.get(i)));
            } else {
                Double value1 = stack.pop();
                switch (Str.get(i)) {
                    case "+":
                        stack.push(stack.pop() + value1);
                        break;
                    case "-":
                        stack.push(stack.pop() - value1);
                        break;
                    case "/":
                        stack.push(stack.pop() / value1);
                        break;
                    case "*":
                        stack.push(stack.pop() * value1);
                        break;
                }
            }

        }

        return stack.pop();
    }

    public static String sortingStr(String Str) {
        List<String> Out = new ArrayList();
        Stack<String> stack = new Stack<>();
        int Index = 0;
        for (int i = 0; i < Str.length(); i++) {

            if (!(MAIN_MATH_OPERATIONS.containsKey(Character.toString(Str.charAt(i))) || NUMBER.contains(Character.toString(Str.charAt(i))))) {
                return null;
            }
            if (NUMBER.contains(Character.toString(Str.charAt(i)))) {
                if (i == (Str.length() - 1)) Out.add(Str.substring(Index, i + 1));
                continue;
            } else if (i != 100) {
                if (Index != i) Out.add(Str.substring(Index, i));
                if (stackADD(stack, Str.charAt(i))) {
                    stack.push(Character.toString(Str.charAt(i)));
                } else if (Character.toString(Str.charAt(i)).equals(")")) {

                    while (!stack.peek().equals("(")) {
                        Out.add(stack.peek());
                        stack.pop();
                        if (stack.isEmpty()) return null;
                    }
                    stack.pop();
                } else {
                    while (MAIN_MATH_OPERATIONS.get(stack.peek()) >= MAIN_MATH_OPERATIONS.get(Character.toString(Str.charAt(i)))) {
                        Out.add(stack.peek());
                        stack.pop();
                        if (stack.isEmpty()) break;
                    }
                    stack.push(Character.toString(Str.charAt(i)));
                }

            } else return null;
            Index = i + 1;
        }
        while (!stack.isEmpty()) {
            Out.add(stack.peek());
            stack.pop();
        }
        System.out.println('\n');
        for (int i = 0; i < Out.size(); i++) {
        }
        return Double.toString(computation(Out));
    }


    public  String evaluate(String InStr) {
        String OutStr = "";
        InStr = InStr.replaceAll("\\s", "");
       for (int i=0;i<InStr.length();i++){
           if(MAIN_MATH_OPERATIONS.containsKey(Character.toString(InStr.charAt(i)))||InStr.charAt(i)=='.'){
               if(MAIN_MATH_OPERATIONS.containsKey(Character.toString(InStr.charAt(i+1)))||InStr.charAt(i+1)=='.')return null;
           }
       }
        OutStr = sortingStr(InStr);
        double AnswerDouble = 0.0;
        if (OutStr != null) {
            AnswerDouble = Math.round(Double.parseDouble(OutStr) * 10000) / 10000D;
            if (AnswerDouble % 1 != 0) return (String.valueOf(AnswerDouble));
            else return String.valueOf((int)(AnswerDouble));
        } else return OutStr;
    }

}
